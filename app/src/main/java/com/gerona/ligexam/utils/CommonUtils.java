package com.gerona.ligexam.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtils {
        private static final String TAG = "CommonUtils";
        private CommonUtils() {
            // This utility class is not publicly instantiable
        }
    public static void showOrHideKeyboard(View view, Context context, boolean showHide){
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(showHide){
            imm.showSoftInput(view, 0);
            view.requestFocus();
        }
        else
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }
    public static Date parseDate(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = null;
        try {
             d = sdf.parse(date);
        } catch (ParseException ex) {
            Log.v("Exception", ex.getLocalizedMessage());
        }
        return  d;
    }
    public static AlertDialog.Builder showAlertDialog(Context context, String title, String message, int drawableId) {
        AlertDialog.Builder alertDialog = null;
        if (context != null) {
            alertDialog = new AlertDialog.Builder(context);
            alertDialog.setTitle(title);
            alertDialog.setMessage(message);
        }
        return alertDialog;
    }
    public static SpannableString underlinedText(String cont){
        SpannableString content = new SpannableString( cont ) ;
        content.setSpan( new UnderlineSpan() , 0 , content.length() , 0 ) ;
        return  content;
    }

}
