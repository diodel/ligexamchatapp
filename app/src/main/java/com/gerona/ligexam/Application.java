package com.gerona.ligexam;

import com.gerona.ligexam.utils.TypefaceUtil;

public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "arial.ttf");
    }
}
