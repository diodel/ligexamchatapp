package com.gerona.ligexam.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gerona.ligexam.utils.AppConstant;
import com.gerona.ligexam.R;
import com.gerona.ligexam.utils.SharedPref;
import com.gerona.ligexam.model.Chat;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_ME = 1;
    private static final int VIEW_TYPE_OTHER = 2;
    List<Chat> mChats;

    public ChatAdapter(List<Chat> mChats) {
        this.mChats = mChats;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_ME:
                View viewChatMine = layoutInflater.inflate(R.layout.item_messages, parent, false);
                viewHolder = new MyChatViewHolder(viewChatMine);
                break;
            case VIEW_TYPE_OTHER:
                View viewChatOther = layoutInflater.inflate(R.layout.item_messages_others, parent, false);
                viewHolder = new OtherChatViewHolder(viewChatOther);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (mChats.get(position).getSenderUid().equals(SharedPref.read(AppConstant.USER_ID,""))) {
            configureMyChatViewHolder((MyChatViewHolder) holder, position);
        } else {
            configureOtherChatViewHolder((OtherChatViewHolder) holder, position);
        }
    }


    private void configureMyChatViewHolder(final MyChatViewHolder myChatViewHolder, int position) {
        Chat chat = mChats.get(position);
        myChatViewHolder.tvSender.setText(chat.getSender());
        myChatViewHolder.tvChat.setText(chat.getMessage());


    }

    private void configureOtherChatViewHolder(final OtherChatViewHolder otherChatViewHolder, int position) {
        final Chat chat = mChats.get(position);
        otherChatViewHolder.tvOtherSender.setText(chat.getSender());
        otherChatViewHolder.tvOtherChat.setText(chat.getMessage());
    }

    @Override
    public int getItemCount() {
        return mChats.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (mChats.get(position).getSenderUid().equals(SharedPref.read(AppConstant.USER_ID,""))) {
            return VIEW_TYPE_ME;
        } else
            return VIEW_TYPE_OTHER;
    }

    private static class MyChatViewHolder extends RecyclerView.ViewHolder {
        private TextView tvChat, tvSender;

        public MyChatViewHolder(View itemView) {
            super(itemView);
            tvChat = (TextView) itemView.findViewById(R.id.tv_chat);
            tvSender = (TextView) itemView.findViewById(R.id.tv_name);
        }
    }

    private static class OtherChatViewHolder extends RecyclerView.ViewHolder {
        private TextView tvOtherChat, tvOtherSender;

        public OtherChatViewHolder(View itemView) {
            super(itemView);
            tvOtherChat = (TextView) itemView.findViewById(R.id.tv_others_chat);
            tvOtherSender = (TextView) itemView.findViewById(R.id.tv_others_name);
        }
    }
}
