package com.gerona.ligexam.model;

public class Chat {
    private String sender;
    private String senderUid;
    private String message;
    private long dateSend;

    public Chat(){

    }

    public Chat(String sender, String senderUid, String message, long dateSend) {
        this.sender = sender;
        this.senderUid = senderUid;
        this.message = message;
        this.dateSend = dateSend;
    }

    public long getDateSend() {
        return dateSend;
    }

    public void setDateSend(long dateSend) {
        this.dateSend = dateSend;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderUid() {
        return senderUid;
    }

    public void setSenderUid(String senderUid) {
        this.senderUid = senderUid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}