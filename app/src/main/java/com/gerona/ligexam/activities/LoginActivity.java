package com.gerona.ligexam.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gerona.ligexam.utils.AppConstant;
import com.gerona.ligexam.utils.CommonUtils;
import com.gerona.ligexam.R;
import com.gerona.ligexam.utils.SharedPref;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etUsername,etPassword;
    private TextView tvUsernameError,tvPasswordError,tvLogIn,tvSignUp;
    private boolean isUserExist;
    private FirebaseDatabase userDatabase;
    private DatabaseReference userRef;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initializeView();

        SharedPref.init(this);

        userDatabase = FirebaseDatabase.getInstance();
        userRef = FirebaseDatabase.getInstance().getReference();

        tvSignUp.setText(CommonUtils.underlinedText(getString(R.string.sign_up)));

    }

    private void initializeView(){
        etUsername = (EditText)findViewById(R.id.et_user_name);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        etPassword = (EditText)findViewById(R.id.et_password);
        tvUsernameError = (TextView) findViewById(R.id.tv_user_name_error);
        tvPasswordError = (TextView) findViewById(R.id.tv_password_error);
        tvLogIn = (TextView) findViewById(R.id.tv_login);
        tvSignUp = (TextView) findViewById(R.id.tv_sign_up);
        tvSignUp.setOnClickListener(this);
        tvLogIn.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        resetErrorView();
        switch(view.getId()) {
            case R.id.tv_login:
                if(TextUtils.isEmpty(etUsername.getText().toString()) || etUsername.getText().toString().length() < 6 || etUsername.getText().length() > 16){
                    tvUsernameError.setVisibility(View.VISIBLE);
                }else if (TextUtils.isEmpty(etPassword.getText().toString())|| etPassword.getText().toString().length() < 6 || etPassword.getText().length() > 16){
                    tvPasswordError.setVisibility(View.VISIBLE);
                }else {
                    progressBar.setVisibility(View.VISIBLE);
                    checkUserExist(etUsername.getText().toString(),etPassword.getText().toString());
                }
                break;
            case R.id.tv_sign_up:

                Intent intent = new Intent(this, SignupActivity.class);
                startActivity(intent);
                finish();
                break;

        }
    }
    private void resetErrorView(){
        tvPasswordError.setVisibility(View.INVISIBLE);
        tvUsernameError.setVisibility(View.INVISIBLE);
    }
    private void checkUserExist(final String userName, final String password){
        isUserExist = false;
        userRef.child("users").orderByChild("userName").equalTo(userName).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.INVISIBLE);
                String userId = "";
                for(DataSnapshot datas: dataSnapshot.getChildren()){
                    String password1 =datas.child("password").getValue().toString();
                     userId =datas.child("userId").getValue().toString();
                    if(password1.equals(password)){
                        isUserExist = true;
                    }
                }
                if(isUserExist){

                    SharedPref.write(AppConstant.USER_NAME,userName);
                    SharedPref.write(AppConstant.USER_ID,userId);

                    Intent intent = new Intent(LoginActivity.this, ChatActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    CommonUtils.showAlertDialog(LoginActivity.this,"Error","User does not exist",0).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.e("DatabaseError","DatabaseError");
            }
        });
    }
}