package com.gerona.ligexam.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.gerona.ligexam.utils.AppConstant;
import com.gerona.ligexam.R;
import com.gerona.ligexam.utils.CommonUtils;
import com.gerona.ligexam.utils.SharedPref;
import com.gerona.ligexam.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etUsername,etPassword;
    private TextView tvUsernameError,tvPasswordError,tvLogIn,tvSignUp;
    private FirebaseDatabase userDatabase;
    private DatabaseReference userRef;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initializeView();
        SharedPref.init(this);

        userDatabase = FirebaseDatabase.getInstance();
        userRef = FirebaseDatabase.getInstance().getReference("users");
        tvLogIn.setText(CommonUtils.underlinedText(getString(R.string.login)));

    }
    private void initializeView(){
        etUsername = (EditText)findViewById(R.id.et_user_name);
        etPassword = (EditText)findViewById(R.id.et_password);
        tvUsernameError = (TextView) findViewById(R.id.tv_user_name_error);
        tvPasswordError = (TextView) findViewById(R.id.tv_password_error);
        tvLogIn = (TextView) findViewById(R.id.tv_login);
        tvSignUp = (TextView) findViewById(R.id.tv_sign_up);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        tvSignUp.setOnClickListener(this);
        tvLogIn.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        resetErrorView();
        Intent intent = null;
        switch(view.getId()) {
            case R.id.tv_login:
                intent = new Intent(this,LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.tv_sign_up:
                if(TextUtils.isEmpty(etUsername.getText().toString()) || etUsername.getText().toString().length() < 6 || etUsername.getText().length() > 16){
                    tvUsernameError.setVisibility(View.VISIBLE);
                }else if (TextUtils.isEmpty(etPassword.getText().toString())|| etPassword.getText().toString().length() < 6 || etPassword.getText().length() > 16){
                    tvPasswordError.setVisibility(View.VISIBLE);
                }else {
                    progressBar.setVisibility(View.VISIBLE);
                    addNewUserToFirebase(etUsername.getText().toString(),etPassword.getText().toString());
                    intent = new Intent(this, ChatActivity.class);
                    startActivity(intent);
                    finish();
                }
                break;

        }
    }

    private void resetErrorView(){
        tvPasswordError.setVisibility(View.INVISIBLE);
        tvUsernameError.setVisibility(View.INVISIBLE);
    }
    private void addNewUserToFirebase(final String userName, String password) {
        final String userId = userRef.push().getKey();
        User user = new User(userName,password,userId);
        userRef.child(userId).setValue(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Write was successful!
                        //Save user
                        progressBar.setVisibility(View.INVISIBLE);

                        SharedPref.write(AppConstant.USER_NAME,userName);
                        SharedPref.write(AppConstant.USER_ID,userId);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Write failed
                        progressBar.setVisibility(View.INVISIBLE);
                        CommonUtils.showAlertDialog(SignupActivity.this,"Error",e.getLocalizedMessage(),0).show();
                    }
                });

    }
}