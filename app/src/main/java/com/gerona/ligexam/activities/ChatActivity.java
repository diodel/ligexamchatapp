package com.gerona.ligexam.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gerona.ligexam.utils.AppConstant;
import com.gerona.ligexam.utils.CommonUtils;
import com.gerona.ligexam.R;
import com.gerona.ligexam.utils.SharedPref;
import com.gerona.ligexam.adapter.ChatAdapter;
import com.gerona.ligexam.model.Chat;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    private FirebaseDatabase chatDatabase;
    private DatabaseReference chatMessagesRef;
    private RecyclerView rvtMessages;
    private TextView tvSend,tvLogout;
    private EditText etMessage;
    private List<Chat> chatList = new ArrayList<>();
    private ChatAdapter adapter;
    private ProgressBar progressBar;
    private ValueEventListener chatPostListener;
    private ChildEventListener newMessageListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        initializeView();
        SharedPref.init(this);

        chatDatabase = FirebaseDatabase.getInstance();
        chatMessagesRef = FirebaseDatabase.getInstance().getReference("ChatRoom");

        adapter = new ChatAdapter(chatList);
        rvtMessages.setAdapter(adapter);
        LinearLayoutManager layoutManager=new LinearLayoutManager(ChatActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvtMessages.setLayoutManager(layoutManager);

        getAllMessages();
        addNewMessages();

    }
    private void initializeView(){
        tvLogout = (TextView)findViewById(R.id.tv_logout);
        tvLogout.setVisibility(View.VISIBLE);
        rvtMessages = (RecyclerView) findViewById(R.id.recycler_view_chat);
        etMessage = (EditText) findViewById(R.id.edit_text_message);
        tvSend = (TextView) findViewById(R.id.text_message_send);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        tvLogout.setOnClickListener(this);
        tvSend.setOnClickListener(this);
    }
    private void sendNewMessage(String sender_id, String sender, String message) {
        String userId = chatMessagesRef.push().getKey();
        Chat chat = new Chat(sender, sender_id,message,getCurrentDate());
        chatMessagesRef.child(userId).setValue(chat)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        CommonUtils.showAlertDialog(ChatActivity.this,"Error",e.getLocalizedMessage(),0).show();
                    }
                });
        etMessage.setText("");
        CommonUtils.showOrHideKeyboard(tvSend,this,false);
    }
    private void getAllMessages(){
        progressBar.setVisibility(View.VISIBLE);
        chatPostListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                chatList.clear();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Chat chat = child.getValue(Chat.class);
                    chatList.add(chat);
                }
                Collections.sort(chatList, new Comparator<Chat>() {
                    @Override
                    public int compare(Chat u1, Chat u2) {
                        return Long.compare(u1.getDateSend(), u2.getDateSend());
                    }
                });

                adapter.notifyDataSetChanged();
                rvtMessages.smoothScrollToPosition(chatList.size()-1);
                progressBar.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                progressBar.setVisibility(View.INVISIBLE);
                Log.w("Retreive", "loadPost:onCancelled", databaseError.toException());
            }
        };
        chatMessagesRef.addListenerForSingleValueEvent(chatPostListener);
    }

    private void addNewMessages(){
         newMessageListener = new ChildEventListener() {

            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                try {
                Chat chat =  new Chat(dataSnapshot.child("sender").getValue().toString(),dataSnapshot.child("senderUid").getValue().toString(),dataSnapshot.child("message").getValue().toString(),((Long) dataSnapshot.child("dateSend").getValue()).intValue());
                chatList.add(chat);
                adapter.notifyDataSetChanged();
                rvtMessages.smoothScrollToPosition(chatList.size()-1);
                }catch (NullPointerException ex){
                    CommonUtils.showAlertDialog(ChatActivity.this,"Error","Message not sent",0).show();
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("Retreive", "loadPost:onCancelled", databaseError.toException());
            }
        };
        chatMessagesRef.addChildEventListener(newMessageListener);
    }
    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.tv_logout:
                SharedPref.clearAll();
                Intent intent = new Intent(this,SignupActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.text_message_send:
                 if(!TextUtils.isEmpty(etMessage.getText().toString()))
                     sendNewMessage(SharedPref.read(AppConstant.USER_ID,""),SharedPref.read(AppConstant.USER_NAME,""),etMessage.getText().toString());
                 break;

        }
    }
    private long getCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        //Returns current time in millis
       return  calendar.getTimeInMillis();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        chatMessagesRef.removeEventListener(newMessageListener);
        chatMessagesRef.removeEventListener(chatPostListener);
    }
}