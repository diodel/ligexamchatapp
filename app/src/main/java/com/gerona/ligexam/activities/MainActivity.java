package com.gerona.ligexam.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.gerona.ligexam.utils.AppConstant;
import com.gerona.ligexam.R;
import com.gerona.ligexam.utils.SharedPref;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvLogin,tvSignup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeView();
        SharedPref.init(this);
        isAlreadyLogin();

    }

    private void initializeView(){
        tvLogin = (TextView)findViewById(R.id.tv_login);
        tvSignup = (TextView)findViewById(R.id.tv_sign_up);
        tvLogin.setOnClickListener(this);
        tvSignup.setOnClickListener(this);
    }
    private void isAlreadyLogin(){
        if(!TextUtils.isEmpty(SharedPref.read(AppConstant.USER_ID,""))){
            Intent intent = new Intent(this,ChatActivity.class);
            startActivity(intent);
            finish();
        }
    }
    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch(view.getId()) {
            case R.id.tv_login:
                 intent = new Intent(this,LoginActivity.class);
                break;
            case R.id.tv_sign_up:
                intent = new Intent(this,SignupActivity.class);
                break;

        }
        startActivity(intent);
        finish();


    }
}